"use strict";

var output = 0;
var calcMethod = "";
var firstNumber = null;
var secondNumber = null;
var result = null;

function clear(){
    output = 0;
    doOutput(output);
};

function appendNumber(number){
    document.getElementById("output").value = output;
    if (parseInt(output) == 0) {
        output = number;
    } else {
        output = output.toString() + number.toString();
    }
    
    doOutput(output); 
};

function changeSign(){
    output = parseFloat(document.getElementById("output").value);
    output *= -1;
    doOutput(output);
}

function add(a, b){
    return a + b;
}

function subtract(a, b){
    return a - b;
}

function multiply(a, b){
    return a * b;
}

function divide(a, b){
    if (b != 0) {
        return a / b;
    } else {
        return "#DIV/0! error"
    }
    
}

function doOutput(number) {
    document.getElementById("output").value = number;
}

// button clicks

document.getElementById("num-0").onclick = function() {
    appendNumber(0);
};

document.getElementById("num-1").onclick = function() {
    appendNumber(1);
};

document.getElementById("num-2").onclick = function() {
    appendNumber(2);
};

document.getElementById("num-3").onclick = function() {
    appendNumber(3);
};

document.getElementById("num-4").onclick = function() {
    appendNumber(4);
};

document.getElementById("num-5").onclick = function() {
    appendNumber(5);
};

document.getElementById("num-6").onclick = function() {
    appendNumber(6);
};

document.getElementById("num-7").onclick = function() {
    appendNumber(7);
};

document.getElementById("num-8").onclick = function() {
    appendNumber(8);
};

document.getElementById("num-9").onclick = function() {
    appendNumber(9);
};

document.getElementById("floating-point").onclick = function() {
    
    var input = document.getElementById("output").value;
    const regex = new RegExp("\.");

    if (/\./.test(input)) {
        
    } else {
        appendNumber(".");
    }
};

document.getElementById("clear").onclick = function() {
    clear();
};

document.getElementById("sign").onclick = function() {
    changeSign();
};

document.getElementById("calc-plus").onclick = function() {
    calcMethod = "plus";
    firstNumber = parseFloat(document.getElementById("output").value);
    output = "";
    doOutput(output);
};

document.getElementById("calc-minus").onclick = function() {
    calcMethod = "minus";
    firstNumber = parseFloat(document.getElementById("output").value);
    output = "";
    doOutput(output);
};

document.getElementById("calc-multiply").onclick = function() {
    calcMethod = "multiply";
    firstNumber = parseFloat(document.getElementById("output").value);
    output = "";
    doOutput(output);
};

document.getElementById("calc-divide").onclick = function() {
    calcMethod = "divide";
    firstNumber = parseFloat(document.getElementById("output").value);
    output = "";
    doOutput(output);
};


document.getElementById("equal").onclick = function() {
    
    secondNumber = parseFloat(document.getElementById("output").value);

    switch (calcMethod) {
        case "plus":
            result = add(firstNumber, secondNumber);
            break;

        case "minus":
            result = subtract(firstNumber, secondNumber);
            break;

        case "multiply":
            result = multiply(firstNumber, secondNumber);
            break;

        case "divide":
            result = divide(firstNumber, secondNumber);
            break;
    }


    doOutput(result)

};